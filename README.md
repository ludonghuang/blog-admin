# 前后端分离-个人博客系统
## 项目简介
该博客项目采用SpringBoot+Vue.js实现，采用JWT实现前后端分离验证，项目主要分为前台和后台。
## 技术路线
- 后端框架：SpringBoot 2.3
- 数据库：MySQL 5.7
- 前后端验证技术：JWT、token
- 前端框架：Vuejs、ElementUI
## 实现思路

- 后端：搭建SpringBoot框架，先实现用户的CRUD功能，然后配置拦截器等技术完成用户登录功能。使用postman进行测试。

- 前端：搭建vue-cli脚手架，开发前端项目（登录界面、首页、用户相关的管理页面，与后端程序对接）
- 其他功能模块（前后端配合开发）
## 功能简介
![输入图片说明](https://images.gitee.com/uploads/images/2021/0410/133945_a37d3038_8676079.png "8.png")
## 数据库设计
![输入图片说明](https://images.gitee.com/uploads/images/2021/0410/134306_a0de0219_8676079.png "数据库设计.png")
## 项目截图
#### 前台页面
- 首页
![输入图片说明](https://images.gitee.com/uploads/images/2021/0410/134045_485e6a07_8676079.jpeg "1.jpg")
- 栏目列表页
![输入图片说明](https://images.gitee.com/uploads/images/2021/0410/134105_11fef793_8676079.jpeg "2.jpg")
- 文章详情页
![输入图片说明](https://images.gitee.com/uploads/images/2021/0410/134114_941b0239_8676079.jpeg "3.jpg")
#### 后台页面
- 登录页
![输入图片说明](https://images.gitee.com/uploads/images/2021/0410/134216_07913780_8676079.jpeg "4.jpg")
- 用户管理
![输入图片说明](https://images.gitee.com/uploads/images/2021/0410/134328_20369b51_8676079.jpeg "5.jpg")
- 文章管理
![输入图片说明](https://images.gitee.com/uploads/images/2021/0410/134340_effe5f75_8676079.jpeg "6.jpg")
- 文章编辑（支持MarkDown编辑器）
![输入图片说明](https://images.gitee.com/uploads/images/2021/0410/134412_4264c21d_8676079.jpeg "7.jpg")
