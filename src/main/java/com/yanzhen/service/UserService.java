package com.yanzhen.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yanzhen.entity.User;
import com.yanzhen.mapper.UserDao;
import com.yanzhen.utils.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserDao userDao;

    public int create(User user) {
        return userDao.create(user);
    }

    public int delete(Integer id) {
        return userDao.delete(MapUtils.build().put("id", id).getMap());
    }

    public int update(User user) {
        return userDao.update(MapUtils.build().put("id", user.getId()).beanToMapForUpdate(user));
    }

    public PageInfo<User> query(User user) {
        if (user != null && user.getPage() != null) {
            //startPage(当前页码, 每页显示大小)
            PageHelper.startPage(user.getPage(), user.getLimit());
        }
        //分页查询
        List<User> list = userDao.query(MapUtils.build().beanToMap(user));
        //将查询出来的数据使用PageInfo封装
        PageInfo<User> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }

    public User detail(Integer id) {
        return userDao.detail(MapUtils.build().put("id", id).getMap());
    }

    //用户登录
    public User login(String userName, String password) {
        return userDao.detail(MapUtils.build()
                .put("userName",userName)
                .put("password",password)
                .getMap());
    }

    public int count(User user) {
        return userDao.count(MapUtils.build().beanToMap(user));
    }
}
