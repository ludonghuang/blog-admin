package com.yanzhen.mapper;

import com.yanzhen.entity.Article;

import java.util.List;
import java.util.Map;

public interface ArticleDao {

    int create(Article article);

    int delete(Map<String,Object> paramMap);

    int update(Map<String,Object> paramMap);

    List<Article> query(Map<String,Object> paramMap);

    Article detail(Map<String,Object> paramMap);

    int count(Map<String,Object> paramMap);
}
