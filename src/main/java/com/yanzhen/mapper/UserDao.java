package com.yanzhen.mapper;

import com.yanzhen.entity.User;

import java.util.List;
import java.util.Map;

public interface UserDao {

    int create(User user);

    int delete(Map<String,Object> paramMap);

    int update(Map<String,Object> paramMap);

    List<User> query(Map<String,Object> paramMap);

    User detail(Map<String,Object> paramMap);

    int count(Map<String,Object> paramMap);
}
