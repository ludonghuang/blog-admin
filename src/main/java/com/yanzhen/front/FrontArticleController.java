package com.yanzhen.front;

import com.github.pagehelper.PageInfo;
import com.yanzhen.entity.Article;
import com.yanzhen.service.ArticleService;
import com.yanzhen.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/f/article")
public class FrontArticleController {

    @Autowired
    private ArticleService articleService;

    //查询单个文章
    @GetMapping("/get")
    public Result getById(Integer id){
        Article detail = articleService.detail(id);
        return Result.success(detail);
    }

    //按照栏目ID查询文章列表
    @GetMapping("/getList")
    public Map getByChannelId(Article article){
        PageInfo<Article> page = articleService.query(article);
        return Result.success(page);
    }

    //获取前几条数据
    @GetMapping("/getTop")
    public Result getById(Article article, Integer top){
        List<Article> list = articleService.top(article, top);
        return Result.success(list);
    }

    //获取置顶最新数据
    @GetMapping("/getUp")
    public Map getUp(){
        Article article = new Article();
        article.setTop(1);
        PageInfo<Article> page = articleService.query(article);
        return Result.success(page);
    }
}
