package com.yanzhen.entity;

import com.yanzhen.utils.Entity;
import lombok.Data;

import java.util.Date;

@Data
public class Comment extends Entity {
    private Integer id;         //评论ID

    private String author;      //评论者

    private String email;       //邮箱

    private String ip;          //IP

    private Date createDate;    //创建时间

    private String content;     //评论正文

    private Integer status;     //批准状态 （0待批|1通过|2未通过）

    private Integer articleId;  //文章ID

    private Integer parentId;   //父评论
}