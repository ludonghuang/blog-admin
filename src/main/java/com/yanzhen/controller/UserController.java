package com.yanzhen.controller;

import com.github.pagehelper.PageInfo;
import com.yanzhen.entity.User;
import com.yanzhen.service.UserService;
import com.yanzhen.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.Map;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/create")
    public Result create(@RequestBody User user) {
        userService.create(user);
        return Result.success(user);
    }

    @PostMapping("/delete")
    public Result delete(Integer id) {
        userService.delete(id);
        return Result.success();
    }

    @PostMapping("/update")
    public Result update(@RequestBody User user) {
        userService.update(user);
        return Result.success(user);
    }

    @PostMapping("/query")
    public Map<String,Object> query(@RequestBody User user) {
        PageInfo<User> pageInfo = userService.query(user);
        return Result.success(pageInfo);
    }

    @PostMapping("/detail")
    public Result detail(Integer id) {
        User user = userService.detail(id);
        return Result.success(user);
    }

    @PostMapping("/count")
    public Result count(@RequestBody User user) {
        int count = userService.count(user);
        return new Result(count);
    }
}
