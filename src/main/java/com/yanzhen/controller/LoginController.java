package com.yanzhen.controller;

import com.yanzhen.entity.User;
import com.yanzhen.utils.JWTUtil;
import com.yanzhen.service.UserService;
import com.yanzhen.utils.MapUtils;
import com.yanzhen.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class LoginController {

    @Autowired
    private UserService userService;

    @PostMapping("/login")
    public Result login(@RequestBody Map<String, String> map) {
        String userName = map.get("userName");
        String password = map.get("password");
        User user = userService.login(userName, password);
        if(user != null) {
            //生成token
            String token = JWTUtil.sign(user);
            //响应数据
            Map<String, Object> loginMap = MapUtils.build().put("token", token).put("user", user).getMap();
            return Result.success(loginMap);
        } else {
            return Result.fail("用户名或密码错误");
        }
    }
}
