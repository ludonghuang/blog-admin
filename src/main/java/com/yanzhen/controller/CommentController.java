package com.yanzhen.controller;

import com.github.pagehelper.PageInfo;
import com.yanzhen.entity.Comment;
import com.yanzhen.service.CommentService;
import com.yanzhen.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/comment")
public class CommentController {

    @Autowired
    private CommentService commentService;

    @PostMapping("/create")
    public Result create(@RequestBody Comment comment) {
        commentService.create(comment);
        return Result.success(comment);
    }

    @PostMapping("/delete")
    public Result delete(Integer id) {
        commentService.delete(id);
        return Result.success();
    }

    @PostMapping("/update")
    public Result update(@RequestBody Comment comment) {
        commentService.update(comment);
        return Result.success(comment);
    }

    @PostMapping("/query")
    public Map<String,Object> query(@RequestBody Comment comment) {
        PageInfo<Comment> pageInfo = commentService.query(comment);
        return Result.success(pageInfo);
    }

    @PostMapping("/detail")
    public Result detail(Integer id) {
        Comment comment = commentService.detail(id);
        return Result.success(comment);
    }

    @PostMapping("/count")
    public Result count(@RequestBody Comment comment) {
        int count = commentService.count(comment);
        return new Result(count);
    }
}
