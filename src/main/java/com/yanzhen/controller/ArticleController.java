package com.yanzhen.controller;

import com.github.pagehelper.PageInfo;
import com.yanzhen.entity.Article;
import com.yanzhen.service.ArticleService;
import com.yanzhen.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/article")
public class ArticleController {

    @Autowired
    private ArticleService articleService;

    @PostMapping("/create")
    public Result create(@RequestBody Article article) {
        articleService.create(article);
        return Result.success(article);
    }

    @PostMapping("/delete")
    public Result delete(Integer id) {
        articleService.delete(id);
        return Result.success();
    }

    @PostMapping("/update")
    public Result update(@RequestBody Article article) {
        articleService.update(article);
        return Result.success(article);
    }

    @PostMapping("/query")
    public Map<String,Object> query(@RequestBody Article article) {
        PageInfo<Article> pageInfo = articleService.query(article);
        return Result.success(pageInfo);
    }

    @PostMapping("/detail")
    public Result detail(Integer id) {
        Article article = articleService.detail(id);
        return Result.success(article);
    }

    @PostMapping("/count")
    public Result count(@RequestBody Article article) {
        int count = articleService.count(article);
        return new Result(count);
    }
}
